
// Deps
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

// Get api routes
const api = require('./routes/api');

// Declare app
const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Cross Origin middleware
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*")
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
//     next()
//   })
  
// Set the first route
app.use('/', api);

// Get port from environment and store in Express
const port = process.env.PORT || '3000';
app.set('port', port);

// Create HTTP server
const server = http.createServer(app);

// Listen on provided port
server.listen(port, () => console.log(`API running on localhost:${port}`));
