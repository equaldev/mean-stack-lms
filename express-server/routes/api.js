const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const host = 'mongodb://database/mean-docker';

// Connect to mongo db container
mongoose.connect(host);

// Mongoose schema
const userSchema = new mongoose.Schema({
    name: String,
    age: Number
})

// Create mongoose model
const User = mongoose.model('User', userSchema);

// GET '/' api listing
router.get('/', (req, res) => {
    res.send('API works, yaaay');
})

router.get('/users', (req, res) => {
    User.find({}, (err,users) =>  {

        if (err) {
            res.status(500).send(error);
        }

        res.send(200).json(users);


    })   
})

router.get('/users/:id', (req, res) => {
    User.findById(req.param.id, (err,users) =>  {

        if (err) {
            res.status(500).send(error);
        }

        res.send(200).json(users);


    });   
});


router.post('/users', (req, res) => {
    let user = new User({
        name: req.body.name,
        age: req.body.age
    });

    user.save(error => {
        if (error) {
            res.status(500).send(error);
        }

        res.status(201).json({
            message: 'User created successfully!'
        });

    });
});



module.exports = router;